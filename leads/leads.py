from selenium.webdriver.support import expected_conditions as EC

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Keys, ActionChains
from selenium.webdriver.common.by import By
import time
import string
import random
from selenium.webdriver.support.wait import WebDriverWait

def add_leads(driver,dict):
    try:
        leads_elem = driver.find_element(By.XPATH, dict.get('leads_elem'))
        assert leads_elem.is_displayed()
        leads_elem.click()

        driver.find_element(By.XPATH, dict.get('add_leads_button')).click()
        S = 10
        ran = ''.join(random.choices(string.ascii_lowercase + string.digits, k=S))
        email_id=str(ran)+'@gmail.com'
        driver.find_element(By.NAME, dict.get('email_textbox')).send_keys(email_id)
        time.sleep(2)

        #element = WebDriverWait(driver, 10).until(
        #    EC.visibility_of_element_located((By.ID, "scrollableDiv"))
        #)

        driver.find_element(By.XPATH, dict.get('create_lead')).click()

        recruit_elem = driver.find_element(By.XPATH, dict.get('recruit_elem'))
        recruit_elem.send_keys(Keys.PAGE_UP)

        time.sleep(0.5)

        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, dict.get('success_totast_class')))
        )
        #notification = driver.find_element(By.CLASS_NAME, 'ant-notification-notice-message')
        element.is_displayed()
        assert 'Success' == element.text

    except NoSuchElementException as e:
        assert False
    except AssertionError as ae:
        assert False
    except TimeoutError as te:
        assert False

#negative test case, for trying to add lead without email id
def add_lead_without_email(driver,dict):
    try:
        leads_elem = driver.find_element(By.XPATH, dict.get('leads_elem'))
        assert leads_elem.is_displayed()
        leads_elem.click()

        driver.find_element(By.XPATH, dict.get('add_leads_button')).click()

        recruit_elem = driver.find_element(By.XPATH, dict.get('recruit_elem'))
        recruit_elem.send_keys(Keys.ENTER) #Just to focus on any element



        driver.find_element(By.XPATH, dict.get('create_lead')).click()

        recruit_elem.send_keys(Keys.PAGE_UP)

        time.sleep(0.5)

        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, dict.get('success_totast_class')))
        )
        #notification = driver.find_element(By.CLASS_NAME, 'ant-notification-notice-message')
        element.is_displayed()
        assert 'Error' == element.text

    except NoSuchElementException as e:
        assert False
    except AssertionError as ae:
        assert False
    except TimeoutError as te:
        assert False


