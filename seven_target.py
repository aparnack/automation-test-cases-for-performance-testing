import time

from assistant.assistant import edit_assistant
from dashboard.dashboard import verify_dashboard
from leads.leads import add_leads, add_lead_without_email
from common.login import perform_login
from orgmsg.org_messages import create_new_schedule, change_org_name

import csv


dict = {}

file = open('data.csv','r')
csv_reader = csv.reader(file)
rows = list(csv_reader)

for row in rows:
    if row[0] in dict:
        dict[row[0]].append(row[1])
    else:
        dict[row[0]] = row[1]


def test_dashboard(setup):
    #driver = webdriver.Chrome(executable_path='E:/Driver/chromedriver.exe')
    #driver.maximize_window()
    #driver.get("https://solution-qa.7targets.com/")
    driver = setup["driver"]
    url = setup["url"]
    driver.get(url)
    time.sleep(5)
    perform_login(driver, dict)
    time.sleep(10)
    verify_dashboard(driver,dict)
    driver.close()

def test_add_leads(setup):
    driver = setup["driver"]
    url = setup["url"]
    driver.get(url)
    time.sleep(5)
    perform_login(driver, dict)
    time.sleep(10)
    add_leads(driver,dict)
    driver.close()

def test_edit_assitant(setup):
    driver = setup["driver"]
    url = setup["url"]
    driver.get(url)
    time.sleep(5)
    perform_login(driver, dict)
    time.sleep(10)
    edit_assistant(driver,dict)
    driver.close()

def test_create_new_schedule(setup):
    driver = setup["driver"]
    url = setup["url"]
    driver.get(url)
    time.sleep(5)
    perform_login(driver, dict)
    time.sleep(10)
    create_new_schedule(driver,dict)
    driver.close()

def test_change_org_name(setup):
    driver = setup["driver"]
    url = setup["url"]
    driver.get(url)
    time.sleep(5)
    perform_login(driver,dict)
    time.sleep(10)
    change_org_name(driver,dict)
    driver.close()

def test_add_lead_without_email(setup):
    driver = setup["driver"]
    url = setup["url"]
    driver.get(url)
    time.sleep(5)
    perform_login(driver, dict)
    time.sleep(10)
    add_lead_without_email(driver, dict)
    driver.close()