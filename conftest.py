import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="chrome")
    parser.addoption("--url", action="store", default="https://solution-qa.7targets.com/")

@pytest.fixture()
def setup(pytestconfig):
    # chrome_options = webdriver.ChromeOptions()
    # driver = webdriver.Chrome(ChromeDriverManager().download_and_install())
    # s = Service(r"C:\Users\AAIC\Downloads\chromedriver_win32\chromedriver.exe")
    # driver = webdriver.Chrome(service=s)
    if pytestconfig.getoption("browser").lower() == "chrome":
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        #chromedriver_path =Service(r"/usr/local/bin/chromedriver")
        chromedriver_path = Service(r"/usr/local/bin/chromedriver")
        # s = Service(r"C:\Users\AAIC\Downloads\chromedriver_win32\chromedriver.exe")
        driver = webdriver.Chrome(service= chromedriver_path,options=options)
        driver.maximize_window()

    elif pytestconfig.getoption("browser").lower() == "firefox":
        options = webdriver.FirefoxOptions()
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        driver_path =Service(r"/usr/bin/geckodriver")
        # driver_path =Service(r"C:\Users\AAIC\Downloads\geckodriver-v0.30.0-win64\geckodriver.exe")
        driver = webdriver.Chrome(service= driver_path,options=options)
        driver.maximize_window()

    yield {"driver": driver, "url": pytestconfig.getoption("url")}
