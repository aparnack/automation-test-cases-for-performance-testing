from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def perform_login(driver, dict):
    driver.find_element(By.ID, dict.get('username').split('~')[0]).send_keys(dict.get('username').split('~')[1])
    driver.find_element(By.ID, dict.get('password').split('~')[0]).send_keys(dict.get('password').split('~')[1])
    driver.find_element(By.XPATH, dict.get('login_button')).click()
    element = WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.CLASS_NAME, dict.get('success_totast_class')))
    )
    element.is_displayed()
    assert 'Success' == element.text