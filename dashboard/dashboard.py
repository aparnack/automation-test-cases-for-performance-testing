from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time


#dashboard- click on total users

def verify_dashboard(driver,dict):
    try:
        dashboard_engage_elem = driver.find_element(By.XPATH, dict.get('dashboard_engage_elem'))
        assert dashboard_engage_elem.is_displayed()
        dashboard_total_leads = driver.find_element(By.XPATH, dict.get('dashboard_total_leads'))
        assert dashboard_total_leads.is_displayed()
        dashboard_total_leads.click()
        time.sleep(5)
        leads_tab = driver.find_element(By.XPATH, dict.get('leads_tab'))
        assert leads_tab.is_displayed()

    except NoSuchElementException:
        assert False
    except AssertionError:
        assert False

#assert 'Engaged Leads1233' in driver.page_source