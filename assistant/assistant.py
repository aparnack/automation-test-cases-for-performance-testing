from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time
import pdb

from selenium.webdriver.support.wait import WebDriverWait

def edit_assistant(driver,dict):
    try:
        assistant_div = driver.find_element(By.XPATH,dict.get('assistant'))
        assert assistant_div.is_displayed()
        assistant_div.click()

        time.sleep(5)
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        assistant_name = driver.find_element(By.XPATH,dict.get('assistant_name'))
        assistant = assistant_name.text

        ### Dropdown
        time.sleep(5)

        element = driver.find_element(By.XPATH,dict.get('edit_assistant_button'))
        actions = ActionChains(driver)
        actions.move_to_element(element)
        actions.click()
        actions.perform()

        time.sleep(2)

        assistant_list_textbox = driver.find_element(By.ID, dict.get('assistant_list_textbox'))
        assistant_list_textbox.click()

        select_name_dropdown = driver.find_element(By.CLASS_NAME,dict.get('drop_down_list_class'))
        assert select_name_dropdown.is_displayed()

        time.sleep(5)
        if(assistant == 'Ashley'):
            driver.find_element(By.XPATH,dict.get('assist_andy_path')).click()  ##Ashley XPATH
        else:
            driver.find_element(By.XPATH,dict.get('assist_ashley_path')).click() ##Ashley XPATH

        driver.find_element(By.XPATH,dict.get('update_button')).click()

        #Store name which we have selected form dropdown and assert with current_name

        time.sleep(1)

        #element = WebDriverWait(driver, 10).until(
        #    EC.visibility_of_element_located((By.CLASS_NAME, dict.get('success_totest_class')))
        #)
        # notification = driver.find_element(By.CLASS_NAME, 'ant-notification-notice-message')
        #element.is_displayed()
        #assert 'Success' == element.text

        leads_elem = driver.find_element(By.XPATH, dict.get('leads_elem'))
        assert leads_elem.is_displayed()
        leads_elem.click()

        time.sleep(2)

        assistant_div = driver.find_element(By.XPATH, dict.get('assistant'))
        assert assistant_div.is_displayed()
        assistant_div.click()

        time.sleep(2)
        assistant_name = driver.find_element(By.XPATH,dict.get('assistant_name'))
        if assistant == 'Ashley' :
            assert 'Andy' == assistant_name.text
        else:
            assert 'Ashley' == assistant_name.text

    except Exception as e:
        assert False
    except AssertionError as ae:
        assert False
     #   print('Error occurred')


