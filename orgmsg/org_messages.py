import string
import random


from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import time

from selenium.webdriver.support.wait import WebDriverWait


def create_new_schedule(driver,dict):
    try:
        org_messages_elem = driver.find_element(By.LINK_TEXT, dict.get('org_messages_elem'))
        assert org_messages_elem.is_displayed()
        org_messages_elem.click()

        time.sleep(2)
        custom_messages = driver.find_element(By.LINK_TEXT, dict.get('custom_messages'))
        assert custom_messages.is_displayed()
        custom_messages.click()

        time.sleep(2)
        create_custom_msg = driver.find_element(By.XPATH, dict.get('create_custom_msg'))
        assert create_custom_msg.is_displayed()
        create_custom_msg.click()

        time.sleep(5)

        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, dict.get('add_schedule')))))

        time.sleep(5)

        S = 10
        ran = ''.join(random.choices(string.ascii_lowercase + string.digits, k=S))
        schedule_name = str(ran)

        create_custom_msg_text = driver.find_element(By.CSS_SELECTOR, dict.get('create_custom_msg_text'))

        actions = ActionChains(
            driver)  # ActionChains are used to perform similar steps like we used to do manually on webpage for e.g. focus on dropdown, click on dropdown to open list, select a value
        actions.move_to_element(create_custom_msg_text)
        actions.click()
        actions.send_keys(schedule_name)
        actions.perform()


        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, dict.get('add_schedule_drawer')))))
        #time.sleep(30)

        time.sleep(5)

        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, dict.get('success_totast_class')))
        )
        element.is_displayed()
        assert 'Success' == element.text

        assert schedule_name in driver.page_source

    except NoSuchElementException:
        assert False
    except AssertionError:
        assert False

def change_org_name(driver,dict):
    try:
        org_messages_elem = driver.find_element(By.LINK_TEXT, 'Org Messages')
        assert org_messages_elem.is_displayed()
        org_messages_elem.click()

        time.sleep(2)
        org_details_elem = driver.find_element(By.XPATH, dict.get('org_details_elem'))
        assert org_details_elem.is_displayed()
        org_details_elem.click()


        S = 10
        ran = ''.join(random.choices(string.ascii_lowercase + string.digits, k=S))
        org_name = str(ran)
        org_name_elem = driver.find_element(By.ID,'orgName')
        assert org_name_elem.is_displayed()
        org_name_elem.clear()
        org_name_elem.send_keys(org_name)
        time.sleep(2)

        M = 10
        ran = ''.join(random.choices(string.ascii_lowercase + string.digits, k=M))
        org_url = 'https://www.' + str(ran) + '.com'
        org_url_elem = driver.find_element(By.ID, 'orgUrl')
        assert org_url_elem.is_displayed()
        for i in range(0, 100):
            org_url_elem.send_keys(Keys.BACKSPACE)

        time.sleep(2)
        org_url_elem.send_keys(org_url)


        org_details_update = driver.find_element(By.XPATH, dict.get('org_details_update'))
        org_details_update.is_displayed()
        org_details_update.click()

        time.sleep(5)

        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, dict.get('success_totast_class')))
        )
        element.is_displayed()
        assert 'Success' == element.text

        #assert org_name_elem in driver.page_source


    except NoSuchElementException:
        assert False
    except AssertionError:
        assert False