to Run a python file:
py filename.py

Pytest framework commands:

pytest seven_target.py  (#for normal run without report)
pytest seven_targets::test_login (::<test name> to run a specific testcase)

pytest seven_target.py --html=report.html (# for generating html report)
pytest seven_target.py --json-report --json-report-summary (# for generating json report)

pytest seven_target.py --json-report --json-report-file=reports/report.json  --html=reports/report.html (for both report generation)

pytest seven_target.py --json-report --json-report-file=reports/report.json --html-report=./reports/report.html --title='7Targets'  (# for both reports & report from html reporter)